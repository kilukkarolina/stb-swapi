package planets;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetTest extends BaseTest {

    @Test
    public void readAllPlanets() {

        Response response = given()
                .spec(reqSpec)
                .when()
                .get(BASE_URL + "/" + PLANETS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getInt("count")).isEqualTo(61);

    }

    @Test
    public void readOnePlanet() {

        Response response = given()
                .spec(reqSpec)
                .when()
                .get(BASE_URL + "/" + PLANETS + "/1")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("Tatooine");
        assertThat(json.getString("rotation_period")).isEqualTo("23");
        assertThat(json.getString("orbital_period")).isEqualTo("304");
        assertThat(json.getString("terrain")).isEqualTo("desert");
        assertThat(json.getString("population")).isEqualTo("200000");

        List<String> residents = json.getList("residents");
        assertEquals(10, residents.size());

    }

    @Test
    public void readPlanetsUsingSearchParam() {

        Response response = given()
                .spec(reqSpec)
                .queryParam("search", "ant")
                .when()
                .get(BASE_URL + "/" + PLANETS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getInt("count")).isEqualTo(3);

        List<String> namesList = json.getList("results.name");
        assertThat(namesList).hasSize(3).contains("Coruscant", "Dantooine", "Ord Mantell");

    }

    @DisplayName("Read planets using full name search")
    @ParameterizedTest(name = "Name: {0}")
    @MethodSource("readPlanetFullNameData")
    public void readPlanetUsingFullNameSearchParam(String fullName) {

        Response response = given()
                .spec(reqSpec)
                .queryParam("search", fullName)
                .when()
                .get(BASE_URL + "/" + PLANETS)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getInt("count")).isEqualTo(1);

        List<String> namesList = json.getList("results.name");
        assertThat(namesList).hasSize(1).contains(fullName);

    }

    private static Stream<Arguments> readPlanetFullNameData() {
        return Stream.of(
                Arguments.of("Yavin IV"),
                Arguments.of("Endor"),
                Arguments.of("Kamino"));
    }

}
