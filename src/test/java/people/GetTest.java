package people;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetTest extends BaseTest {

    @Test
    public void readAllPeople() {

        Response response = given()
                .spec(reqSpec)
                .when()
                .get(BASE_URL + "/" + PEOPLE)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getInt("count")).isEqualTo(87);
        List<String> names = json.getList("results.name");
        assertEquals(10, names.size());

    }

    @Test
    public void readOnePerson() {

        Response response = given()
                .spec(reqSpec)
                .when()
                .get(BASE_URL + "/" + PEOPLE + "/1")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo("Luke Skywalker");
        assertThat(json.getString("height")).isEqualTo("172");
        assertThat(json.getString("mass")).isEqualTo("77");
        assertThat(json.getString("hair_color")).isEqualTo("blond");
        assertThat(json.getString("gender")).isEqualTo("male");

        List<String> films = json.getList("films");
        assertEquals(5, films.size());

    }

    @Test
    public void readPeopleUsingSearchParam() {

        Response response = given()
                .spec(reqSpec)
                .queryParam("search", "Dar")
                .when()
                .get(BASE_URL + "/" + PEOPLE)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getInt("count")).isEqualTo(3);

        List<String> namesList = json.getList("results.name");
        assertThat(namesList).hasSize(3).contains("Darth Vader", "Biggs Darklighter", "Darth Maul");

    }

    @DisplayName("Read person using full name search")
    @ParameterizedTest(name = "Name: {0}")
    @MethodSource("readPersonFullNameData")
    public void readPersonUsingFullNameSearchParam(String fullName) {

        Response response = given()
                .spec(reqSpec)
                .queryParam("search", fullName)
                .when()
                .get(BASE_URL + "/" + PEOPLE)
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getInt("count")).isEqualTo(1);

        List<String> namesList = json.getList("results.name");
        assertThat(namesList).hasSize(1).contains(fullName);

    }

    private static Stream<Arguments> readPersonFullNameData() {
        return Stream.of(
                Arguments.of("Darth Vader"),
                Arguments.of("R2-D2"),
                Arguments.of("Owen Lars"));
    }


}
